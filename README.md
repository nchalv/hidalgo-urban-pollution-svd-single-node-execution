  ## Execution instructions for the SVD calculation code in a single-node spark installation

  This code has been developeed by ICCS per the requirements of the Hidalgo project.

  ### Prerequisites
  * Install ``Java JDK``:

          sudo apt install openjdk-11-jdk-headless
  * Download and decompress the Apache Spark tarball:

          wget https://ftp.cc.uoc.gr/mirrors/apache/spark/spark-3.1.2/spark-3.1.2-bin-hadoop3.2.tgz
          tar xvzf spark-3.1.2-bin-hadoop3.2.tgz
          mv spark-3.1.2-bin-hadoop3.2 spark
  * Clone the repository:

          git clone https://gitlab.com/nchalv/hidalgo-urban-pollution-svd-single-node-execution.git
  * Create a directory for the output:
  
          mkdir ./output

  
  ### Code Execution
  Assuming we complete the previous steps as user **ubuntu**...
  
    cd spark
    time bin/spark-submit  --master local[n]  /home/ubuntu/hidalgo-urban-pollution-svd-single-node-execution/simple-project_2.12-1.0.jar -i file:///home/ubuntu/hidalgo-urban-pollution-svd-single-node-execution/data_sample/ -o file:///home/ubuntu/output/output -u -s 10 -a svd --debug-no-transpose

  **Please notice:** ``n`` can be configured so that multiple threads are launched. (For ``n``=2, 2 threads). To use all available threads in your system, set `n`=* (`--master local[*]`) but be careful of the high memory usage that might result to.

  The application output can be found under the ``output`` directory, in the sub-directories ``outputS``, ``outputV``, ``outputU`` respectively.

  In order to relauch the calculation it is required to remove the output directories:
  
    rm -r ~/output/*
